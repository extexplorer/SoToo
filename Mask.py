# coding:utf-8

import numpy as np
import cv2 as cv


def MaskCenterEllipse(size, axesR, fill=True):
	h = size[0];
	w = size[1];
	axesX = int(w * axesR[0]) / 2
	axesY = int(h * axesR[1]) / 2
	center = (int(w * 0.5), int(h * 0.5))
	axesXY = (axesX, axesY)
	mask = np.zeros(size, dtype="uint8")
	isfill = -1 if fill else 2
	cv.ellipse(img=mask, center=center, axes=axesXY, angle=0, startAngle=0, endAngle=360, color=1,
			   thickness=isfill)
	return mask


def MaskRectangle(size, startXYR, WHR, fill=True):
	h = size[0];
	w = size[1];
	startXY = (int(startXYR[0] * w), int(startXYR[1] * h))
	endXY = (startXY[0] + int(WHR[0] * w), startXY[1] + int(WHR[1] * h))
	mask = np.zeros(size, dtype="uint8")
	isfill = -1 if fill else 2
	cv.rectangle(img=mask, pt1=startXY, pt2=endXY, color=1, thickness=isfill)
	return mask



class MaskImages2:
	weight = (0.4, 0.15, 0.15, 0.15, 0.15)
	mask_count = 5

	@staticmethod
	def maskImages(size):
		center_ellip1 = MaskCenterEllipse(size, (0.85, 0.6))
		center_ellip2 = MaskCenterEllipse(size, (0.45, 0.85))
		sum_ellip = cv.add(center_ellip1, center_ellip2)
		new_sum_ellip = sum_ellip
		startXY = ((0, 0), (0.5, 0), (0, 0.5), (0.5, 0.5))
		corner_rects = tuple(MaskRectangle(size, sXYR, (0.5, 0.5)) for sXYR in startXY)
		corner_masks = tuple(cv.subtract(corner_rect, new_sum_ellip) for corner_rect in corner_rects)
		return (new_sum_ellip,) + corner_masks


class MaskImages1:
	weight = (0.2, 0.2, 0.2, 0.2, 0.2)
	mask_count = 5

	@staticmethod
	def maskImages(size):
		center_ellip = MaskCenterEllipse(size, (0.7, 0.7))
		startXY = ((0, 0), (0.5, 0), (0, 0.5), (0.5, 0.5))
		corner_rects = tuple(MaskRectangle(size, sXYR, (0.5, 0.5)) for sXYR in startXY)
		corner_masks = tuple(cv.subtract(corner_rect, center_ellip) for corner_rect in corner_rects)
		return (center_ellip,) + corner_masks

class Mask:
	CenterEllipse = 0
	CenterCrossEllipse = 1
	mask_method = (MaskImages1, MaskImages2)
