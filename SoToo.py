# coding:utf-8

from Retrieval import *
from Feature import *
import getopt, sys
from Mask import *


def usage():
	print u"-i,--ftrfile : 特征文件(json格式)"
	print u"-d,--imgdir : 图片目录"
	print u"-f,--imgfile : 要搜索的图片文件"
	print u"example:"
	print u"         SoToo -i /foo.json -d /foo : 将foo目录里的所有图片文件提取特征,特征保存在foo.json文件中"
	print u"         SoToo -i /foo.json -f /foo.jpg : 显示foo目录中的图片文件与foo.jpg文件的相似度"
	print u"         必须先提取所有图片的特征,才能比较他们与指定图片文件的的相似度"
def argError():
	print u"参数错误!"
	usage()

try:
	opts, args = getopt.getopt(sys.argv[1:],"hi:d:f:", ["help","ftrfile=","imgfile=","imgdir="])
except getopt.GetoptError, err:
	usage()
	sys.exit(2)
ftrfile_f=False
imgdir_f=False
imgfile_f=False
ftrfile=None
imgdir=None
imgfile=None
for opt, val in opts:
	if opt in ('-i','--ftrfile'):
		ftrfile_f=True
		ftrfile=val
	elif opt in ('-d','--imgdir'):
		imgdir_f=True
		imgdir=val
	elif opt in ('-f','--imgfile'):
		imgfile_f=True
		imgfile=val
	elif opt in ('-h','--help'):
		usage()
		sys.exit(0)

if (imgdir_f and imgfile_f) or not ftrfile_f:
	argError()
	sys.exit(2)

ftrtype=Feature.HSVColorLevel72
if imgdir_f:
	extractFeatures(imgdir=imgdir, ftrfile=ftrfile, ftrtype=ftrtype,maskID=Mask.CenterEllipse)
elif imgfile_f:
	retrieve(imgfile=imgfile, ftrfile=ftrfile, ftrtype=ftrtype,maskID=Mask.CenterEllipse)
else:
	argError()
	sys.exit(2)



