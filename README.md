#SoToo

##python写的入门级CBIR系统   
CBIR:基于内容的图像检索  

**依赖**  
python v2.7.10  
opencv v3.0.0  
numpy v1.9.2  

用法:

1.首先,提取图片库里所有图片的特征:

  `SoToo -i test.json -d imgsdir`  
   提取imgsdir目录下所有图片文件的特征,并保存到特征文件test.json中

2.然后,比较图片库中所有图片文件的与指定文件的相似度:

  `SoToo -i test.json -f img`  
   将会列出imgsdir中所有图片文件与图片img的相似度

  `SoToo -h`    
   显示帮助信息

*写的比较简陋,仅供交流学习*