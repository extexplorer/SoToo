# coding:utf-8

from HSVHistogram import *

def HSVColorLevel72_InitFunc(ftrlist, **kwargs):
	return HSVColorLevel72Ftr(ftr=ftrlist)


def HSVColorLevel32_InitFunc(ftrlist, **kwargs):
	return HSVColorLevel32Ftr(ftr=ftrlist)



class Feature:
	HSVColorLevel72 = 0
	HSVColorLevel32 = 1
	init_methods = (HSVColorLevel72_InitFunc,
					HSVColorLevel32_InitFunc)

	def __init__(self, name):
		self.name = name

	def extract(self, img):
		pass

	def compareWith(self, another):
		pass

	def plot(self):
		pass



class HSVColorFtr(Feature):
	def __init__(self, name, hsvtype):
		Feature.__init__(self, name)
		self.hsvtype = hsvtype
		self.hsv = None
		self.has_norm = False
		self.ftr_norm = 0

	def extract(self, img,mask):
		self.hsv = HSVHistogram(img, self.hsvtype,mask)
		self.feature = self.hsv.hist.tolist()

	def compareWith(self, another):
		if not self.has_norm:
			self.ftr_norm = HSVColorFtr.ftrNorm(self)
		another_ftr_norm=HSVColorFtr.ftrNorm(another)
		sum_norm = self.ftr_norm + another_ftr_norm
		sum = 0
		for i in range(len(self.feature)):
			sum += (self.feature[i]-another.feature[i])**2
		sum**=0.5
		return (1 - sum / sum_norm) * 100

	@staticmethod
	def ftrNorm(ftr):
		sum = 0
		for i in range(len(ftr.feature)):
			sum += ftr.feature[i]**2
		return sum**0.5


class HSVColorLevel72Ftr(HSVColorFtr):
	def __init__(self, ftr=None):
		HSVColorFtr.__init__(self, "HSV Color Level72", HSVQuanMethod.Level72)
		self.feature = ftr


class HSVColorLevel32Ftr(HSVColorFtr):
	def __init__(self, ftr=None):
		HSVColorFtr.__init__(self, "HSV Color Level32", HSVQuanMethod.Level32)
		self.feature = ftr
